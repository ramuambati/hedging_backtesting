# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
from datetime import datetime
from dateutil.relativedelta import relativedelta, FR, TH 


def main():
    global CE_Data
    global PE_Data
    global nifty50
    global Output_Data
    global Entry
    global Entry_date
    global CE_ITM_Depth
    global CE_OTM_Height
    global PE_ITM_Height
    global PE_OTM_Depth
    global CE_OTM_Buy
    global PE_OTM_Buy
    global Entry_spot
    global Entry_position
    global max_idx
    global outfile_path
    global PE_Path
    global CE_Path
    global Nifty_Path


    Entry = 'LT' # LT - Last Friday or LF - Last Friday of the Month
    CE_ITM_Depth  = -1
    CE_OTM_Height = 3
    CE_OTM_Buy = 3
    PE_ITM_Height  = 3
    PE_OTM_Depth = -1
    PE_OTM_Buy = -2
    Entry_position = 'All' 
    input_path = 'C:/Users/ambatir/Desktop/Strategies/August_Expiry_Research/Input/'
    out_path = 'C:/Users/ambatir/Desktop/Strategies/August_Expiry_Research/Output/'
    Nifty_Path = input_path + 'Bank_Nifty_Aug_09_Expiry.csv'
    CE_Path = input_path + 'OPTIDX_BANKNIFTY_CE_27-Jul-2018_TO_09-Aug-2018.csv'
    PE_Path = input_path + 'OPTIDX_BANKNIFTY_PE_27-Jul-2018_TO_09-Aug-2018.csv'
    outfile_path = out_path + 'BANKNIFTY_EXPIRY_AUG09_RESULT.csv'
    #D2D - Sell ATM PE and CE everyday with distant buy
    #All - Take Entire position on day 1 
    
    load_files()
    find_entry_date()
    find_spot_price()
    define_ideal_position()
    determine_daily_position()
    write_output()

def write_output():
    global Output_Data
    global outfile_path
    
    Output_Data.to_csv(outfile_path,index=False)
    print ('Completed..')

def determine_daily_position():
    global Output_Data 
    global Entry_spot
    global Entry_date
    global max_idx

    nifty_idx = len(nifty50)
    min_nifty_idx = nifty50.index[nifty50['Date'] == Entry_date][0] + 1
    print ('nifty_idx = ', nifty_idx)
    print('min nifty idx = ', min_nifty_idx)
    for i in range (min_nifty_idx,nifty_idx):
        close_price = nifty50.at[i,'Close']
        EOD_Date = nifty50.at[i,'Date']
        PE_txt = str(datetime.date(EOD_Date)) + 'PE'
        CE_txt = str(datetime.date(EOD_Date)) + 'CE'
        Zero_Qty = pd.DataFrame(np.zeros((len(Output_Data),2),dtype=float),columns=[CE_txt, PE_txt])
        Output_Data = pd.concat([Output_Data, Zero_Qty], axis=1)
        # Output_Data.columns = pd.concat([Output_Data.columns, CE_txt, PE_txt],axis=1)
        # print (Output_Data.columns)


        temp_df = CE_Data[CE_Data['Date'] == EOD_Date]
        temp_df.columns = CE_Data.columns
        
        for j in range(0,max_idx):
            curr_str = Output_Data.at[j,'StrPrice']
            try:
                price_index = temp_df.index[temp_df['Strike Price'] == curr_str]
                price = temp_df.at[price_index[0],'LTP']
            except IndexError:
                price = 0
            Output_Data.at[j,CE_txt] = Output_Data.at[j,'CEQty'] * price * -1

        temp_df = PE_Data[PE_Data['Date'] == EOD_Date]
        temp_df.columns = PE_Data.columns
        
        for j in range(0,max_idx):
            curr_str = Output_Data.at[j,'StrPrice']
            try:
                price_index = temp_df.index[temp_df['Strike Price'] == curr_str]
                price = temp_df.at[price_index[0],'LTP']
            except IndexError:
                price = 0
            Output_Data.at[j,PE_txt] = Output_Data.at[j,'PEQty'] * price * -1


def define_ideal_position():
    global Output_Data 
    global Entry_spot
    global max_idx

    Zero_Qty = pd.DataFrame(np.zeros((80,6), dtype=float))
    Output_Data = pd.concat([Output_Data, Zero_Qty], axis=1)
    Output_Data.columns = ['StrPrice','CEQty','PEQty','CEPrice','PEPrice','CPremBuyTime','PPremBuyTime']
    Output_Data = Output_Data.drop(Output_Data[Output_Data.StrPrice % 100 != 0].index).reset_index()
    del Output_Data['index']
    # print (Output_Data.shape)
    # print (Output_Data)
    spot_idx = Output_Data.index[Output_Data['StrPrice'] > Entry_spot][0]
    spot_idx = spot_idx - 1
    print ("Index found = ", spot_idx)
    print ('ATM CE = ', Output_Data.at[spot_idx, 'StrPrice'])
    print ('ATM PE = ', Output_Data.at[spot_idx + 1, 'StrPrice'])

    if Entry_position == 'All':
        CE_min_idx = spot_idx + CE_ITM_Depth
        CE_max_idx = spot_idx + CE_OTM_Height
        CE_buy_idx = spot_idx + CE_OTM_Buy
        j = 0

        for i in range(CE_min_idx, CE_max_idx):
            Output_Data.at[i,"CEQty"] = -1
            j += 1

        Output_Data.at[CE_buy_idx,'CEQty'] = j

        PE_min_idx = spot_idx + PE_OTM_Depth
        PE_max_idx = spot_idx + PE_ITM_Height
        PE_buy_idx = spot_idx + PE_OTM_Buy
        j = 0
        for i in range(PE_min_idx, PE_max_idx):
            Output_Data.at[i,"PEQty"] = -1
            j += 1
        Output_Data.at[PE_buy_idx,'PEQty'] = j
    else:
        print ('Need to code for D2D.. come back later..')
        return

    max_idx = len(Output_Data)

    # print ('Length of array = ', max_idx)
    buydate_df = CE_Data[CE_Data['Date'] == Entry_date]
    buydate_df.columns = CE_Data.columns
    # print(buydate_df.shape)
    # print(buydate_df)
    for i in range(0,max_idx):
        curr_str = Output_Data.at[i,'StrPrice']
        try:
            price_index = buydate_df.index[buydate_df['Strike Price'] == curr_str]
            price = buydate_df.at[price_index[0],'LTP']
        except IndexError:
            price = 0
        Output_Data.at[i,'CEPrice'] = Output_Data.at[i,'CEQty'] * price


    buydate_df = PE_Data[PE_Data['Date'] == Entry_date]
    buydate_df.columns = PE_Data.columns
    # print(buydate_df.shape)
    # print(buydate_df)
    for i in range(0,max_idx):
        curr_str = Output_Data.at[i,'StrPrice']
        try:
            price_index = buydate_df.index[buydate_df['Strike Price'] == curr_str]
            price = buydate_df.at[price_index[0],'LTP']
        except IndexError:
            price = 0
        Output_Data.at[i,'PEPrice'] = Output_Data.at[i,'PEQty'] * price

           
def find_spot_price():
    
    global Entry_date
    global Entry_spot

    # row = nifty50.index[nifty50['Date'] == Entry_date][0]
    # print (row)
    # print (nifty50.loc[row])
    # nifty50_entry_row = nifty50.iloc[row]
    # print('Entry Row = ', nifty50_entry_row)
    Entry_spot = nifty50.at[nifty50.index[nifty50['Date'] == Entry_date][0],'Close']
    print('Entry Spot = ', Entry_spot)

def find_entry_date():
    global Entry_date
    print('Expiry = ', CE_Data.at[2,'Expiry'].date())
    Entry_date =  CE_Data.at[2,'Date']
    
    # if Entry == 'LF':
    #     Entry_date = CE_Data.at[2,'Expiry'] + relativedelta(months=(-1), weekday=FR(-1))
    # elif Entry == 'LT':
    #     Entry_date = CE_Data.at[2,'Expiry'] + relativedelta(months=(-1), weekday=TH(-1))
    # else:
    #     Entry_date = CE_Data.at[2,'Expiry'] + relativedelta(months=(-1), weekday=FR(-1))
    #     print ('Entry defaulting to Friday')

    print ('Entry Date = ', Entry_date.date())

def load_files():
    # Get Historical Nifty 50, CE & Option Contract data..
    global CE_Data
    global PE_Data
    global nifty50
    global Output_Data
    global Entry
    global PE_Path
    global CE_Path
    global Nifty_Path
   

    nifty50 = pd.read_csv(Nifty_Path,sep=',')
    nifty50['Date']=pd.to_datetime(nifty50.Date)
    nifty50 = nifty50.sort_values('Date',ascending=True)
    # print ('nifty50 Shape = ', nifty50.shape)

    CE_Data = pd.read_csv(CE_Path,sep=',')
    CE_Data['Date']=pd.to_datetime(CE_Data.Date)
    CE_Data['Expiry']=pd.to_datetime(CE_Data.Expiry)
    CE_Data = CE_Data.sort_values(by=['Date', 'Expiry', 'Strike Price'],ascending=[True,True,True])
    # print ('CE_Data Shape = ', CE_Data.shape)
    
    PE_Data = pd.read_csv(PE_Path,sep=',')
    PE_Data['Date']=pd.to_datetime(PE_Data.Date)
    PE_Data['Expiry']=pd.to_datetime(PE_Data.Expiry)
    PE_Data = PE_Data.sort_values(by=['Date', 'Expiry', 'Strike Price'],ascending=[True,True,True])
    # print ('PE_Data Shape = ', PE_Data.shape)

    # Identify the strike prices from CE_Data

    Output_Data = CE_Data['Strike Price'].drop_duplicates(keep='first').reset_index()
    del Output_Data['index']
    # print ('Strike Price Shape = ', Output_Data.shape)
    # print (Output_Data)

if __name__ == "__main__":
  main()
  
  