# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
import os
import shutil

def main():
    global input_path
    global out_path
    global Output_Data
    global csv_data

    input_path = 'G:\Hedging_Analysis\Downloaded_Data\Stocks'
    out_path = 'G:\Hedging_Analysis\Downloaded_Data\Stocks'
    
    select_files()

    print ('Completed..')


def select_files():

    global input_path
    global csv_data
    entries  = os.scandir(input_path)
    
    for i in entries:
        if i.name.find('CE_1_1_') >= 0:
            input_file = input_path + i.name
            out_file = out_path + i.name.replace('CE_1_1_','CE_01-01-')
            # print (input_file, out_file)
            # shutil.copy(input_file, out_file)
            os.rename(input_file, out_file)
            print ('renamed to ', i.name.replace('CE_1_1_','CE_01-01-'))


   
if __name__ == "__main__":
  main()
  
  