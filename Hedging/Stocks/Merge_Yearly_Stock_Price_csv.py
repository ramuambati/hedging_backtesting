# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
import os
import shutil

def main():
    global input_path
    global out_path
    global Output_Data
    global csv_data
    global stock_list 
    global error_df

    # stock_list = pd.DataFrame({'Symbol': ('ITC', 'MARUTI', 'SBIN', 'RELIANCE', 'ZEEL', 'YESBANK', 'TATAMOTORS','VEDL')})

    # print (stock_list['Symbol'][0])
    input_path = 'G:/Hedging_Analysis/Downloaded_Data/Stocks/'
    out_path = 'G:/Hedging_Analysis/Downloaded_Data_Merged/'
    find_stocks()
    select_files()
    print ("More than 6 files for ", error_df)
    print ('Completed..')


def select_files():

    global input_path
    global csv_data
    global stock_list
    global error_df
    
    
    for j in range(0,len(stock_list)):
    # for j in range(0,20):
        outdf = pd.DataFrame()
        error_df = pd.DataFrame()
        outfile = out_path + stock_list['Symbol'][j] + '_Historical_Data.csv'
        entries  = os.scandir(input_path)
        # print ("for stock", stock_list['Symbol'][j])
        count = 0
        for i in entries:
            if i.name.find((stock_list['Symbol'][j]+"ALLN")) >= 0:
                print ("processing ", i.name)
                input_file = input_path + i.name
                inputdf = pd.read_csv(input_file,sep=',',parse_dates=['Date'])
                inputdf['Date'] = inputdf['Date'].dt.strftime('%Y%m%d')
                outdf = outdf.append(inputdf)
                count = count + 1
            outdf.to_csv(outfile, sep=',', index=False)
        print (count, "files for stock", stock_list['Symbol'][j])
        if count != 6:
            error_df = error_df.append(stock_list['Symbol'][j])

def find_stocks():

    global input_path
    global csv_data
    global stock_list
    
    
    stock_list = pd.DataFrame(columns=["Symbol"])
    entries  = os.scandir(input_path)
    for i in entries:
        parsed_name = i.name[24:]
        parsed_name = parsed_name[:-8]
        print (parsed_name)
        stock_list = stock_list.append({"Symbol" : parsed_name},ignore_index=True)
    
    stock_list = stock_list.drop_duplicates().reset_index()
    print ("number of stocks = ", len(stock_list))
if __name__ == "__main__":
  main()
  
  