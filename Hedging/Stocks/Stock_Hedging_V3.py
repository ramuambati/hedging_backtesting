import pandas as pd
import numpy as np
import os
import sys
import shutil
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta, FR, TH 

global prev_expiry_date
global symbol
global prev_exit_date
global entry_strike, entry_date
global exit_date_df
global CE_buy_price
global exit_stock_price
global detail_output, dd_idx
global ITM_Delta, OTM_Delta
global summary_output, sd_idx
global base
global cost
global stockopt_data, historical_stock_prices
global trailing, trail_cost
global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
global expected_rate
global step_length
global this_expiry_df
global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
global reentry
global corp_actions_file
global corp_actions_df
global from_units, to_units
global stock_list
global nifty_100_stock_list
global nifty_100_stock_list_loc, nifty100
global default_count
global trade_count


def main():
    global prev_expiry_date
    global symbol
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global ITM_Delta, OTM_Delta
    global summary_output, sd_idx
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global reentry
    global corp_actions_file
    global corp_actions_df
    global from_units, to_units
    global stock_list
    global nifty_100_stock_list
    global nifty_100_stock_list_loc, nifty100
    global default_count
    global trade_count
    
    
    dd_idx = 0
    sd_idx = 1

    detail_output = pd.DataFrame(columns=['Symbol', 'Trail/Cost', 'Base', 'ITM_Delta', 'OTM_Delta', 'Expiry','Date', 'Stock Price Entry', 'Stock Price Exit', 'Strike Price', 'Cost','Opt Sell price','Opt Buy price','P/L Stock','P/L Hedge','Net P/L',' TTM low','TTM 25%','TTM 50%','TTM 75%','TTM high'])
    summary_output = pd.DataFrame(columns=['Symbol', 'Trail/Cost', 'Base', 'ITM_Delta', 'OTM_Delta', 'Inv Cost', 'Expiry', 'Trade Count', 'Not Traded Months', 'P/L Stock','P/L Hedge','Net P/L'])

    
    out_doc_name = "All_2014-01-01_2019-06-27_Detail_V3.csv"
    out_doc_summary = "All_2014-01-01_2019-06-27_Summary_V3.csv"
    # corp_actions_file = "Corporate_Actions_Merged.csv"
    data_loc = "C:/Stocks_Data/Hedging_Analysis/Downloaded_Data_Merged/"
    nifty_100_stock_list = 'ind_nifty100list.csv'
    # stock_list = ['RELIANCE', 'ITC', 'SBIN', 'YESBANK', 'ZEEL', 'TATAMOTORS', 'MARUTI', 'VEDL']
    # stock_list = ['RELIANCE', 'ITC', 'SBIN', 'YESBANK', 'TATAMOTORS', 'MARUTI', 'VEDL']
    # stock_list = ['RELIANCE']
    build_stock_list()
    skipped_df = pd.DataFrame(columns=['Symbol'])
    skipped_symbol = pd.DataFrame()

    for symbol in stock_list['Symbol']:
    # for symbol in stock_list:
        print('\007')
        print (datetime.now(), "- Processing started for ", symbol)
        
    
        expected_rate = 1.0
        base = 1        # what entity to pick when it's around 50% TTM
        ITM_Delta = 1   # What entity to pick when it's above 50% TTM
        OTM_Delta = 1   # What entity to pick when it's near TTM low

        load_files()

        count_of_symbols = len(stockopt_data['Symbol'].unique())
        if count_of_symbols > 1:
            skipped_symbol = historical_stock_prices['Symbol'].unique()
            print(skipped_symbol)
            skipped_df = skipped_df.append(pd.DataFrame(skipped_symbol))
            
        else:
            for base in range(2,3):
                for ITM_Delta in range(0,1):
                    for OTM_Delta in range(0,1):
                        default_count = 0
                        trade_count = 0
                        # trail_cost = 0 means selling near cost of entry, adjusted for booked profit
                        # trail_cost = 1 means selling near stock price
                        for trail_cost in range (0,1):
                            summary_output.at[sd_idx,'P/L Stock'] = 0
                            summary_output.at[sd_idx,'P/L Hedge'] = 0
                            summary_output.at[sd_idx,'Net P/L'] = 0
                            summary_output.at[sd_idx,'Inv Cost'] = stockopt_data.at[0,'Stock Price']
                            # cost = entry_stock_price
                            if trail_cost == 1:
                                trailing = 'Stock'
                            else:
                                trailing = "Cost"
                            
                            entry_date = stockopt_data.at[1,"Date"]
                            entry_stock_price = stockopt_data.at[1,"Stock Price"]
                            expiry_date = stockopt_data.at[1,"Expiry"]
                            cost = entry_stock_price
                            # print (datetime.now(), 'entry date = ', entry_date)
                            # print (datetime.now(), 'entry stock price = ', entry_stock_price)
                            # print (datetime.now(), 'expiry_date = ', expiry_date)

                            calc_periods()
                            reentry = 1
                            find_entry_strike()

                            prev_expiry_date = expiry_date
                            exit_date = stockopt_data.at[0, 'Date']
                            daily_check_df = stockopt_data[((stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike) & (stockopt_data['LTP']!=0))].reset_index()

                            for i in range(0, len(stockopt_data)-1):
                                
                                prev_exit_date = exit_date
                                exit_date = stockopt_data.at[i, 'Date']

                                if (stockopt_data.at[i,"Expiry"] != prev_expiry_date):
                                    
                                    expiry_change()
                                    entry_date = prev_expiry_date
                                    entry_stock_price = exit_stock_price
                                    expiry_date = stockopt_data.at[i,"Expiry"]
                                    calc_periods()
                                    reentry = 1
                                    find_entry_strike()
                                    prev_expiry_date = expiry_date
                                    daily_check_df = stockopt_data[((stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike) & (stockopt_data['LTP']!=0))].reset_index()
                                
                                target_up = entry_strike + step_length

                                if (stockopt_data.at[i,"Close Price"] >= target_up):
                                    prev_exit_date = exit_date
                                    expiry_change()
                                    entry_date = prev_exit_date
                                    entry_stock_price = exit_stock_price
                                    expiry_date = stockopt_data.at[i,"Expiry"]
                                    calc_periods()
                                    reentry = 1
                                    find_entry_strike()
                                    prev_expiry_date = expiry_date
                                    daily_check_df = stockopt_data[((stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike) & (stockopt_data['LTP']!=0))].reset_index()
                                
                                # if stockopt_data.at[i,'Date']!=expiry_date:
                                #     LTP_today_df = daily_check_df[(daily_check_df['Date']==stockopt_data.at[i,'Date'])]
                                #     if LTP_today_df.empty != True:
                                #         # print(LTP_today_df)
                                #         LTP_today = float(LTP_today_df["LTP"])
                                #         return_as_of_now = (CE_sell_price - LTP_today)*100/CE_sell_price

                                #         if return_as_of_now >= 75:
                                #             prev_exit_date = exit_date
                                #             expiry_change()
                                #             entry_date = prev_exit_date
                                #             entry_stock_price = exit_stock_price
                                #             expiry_date = stockopt_data.at[i,"Expiry"]
                                #             calc_periods()
                                #             reentry = 1
                                #             find_entry_strike()
                                #             prev_expiry_date = expiry_date
                                #             daily_check_df = stockopt_data[((stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike) & (stockopt_data['LTP']!=0))].reset_index()

                            expiry_change()
                            sd_idx = sd_idx + 1
                        print(datetime.now(), symbol, " completed for base ", base, " by trailing ", trailing, "for ITM Delta of ", ITM_Delta, " and OTM Delta of ", OTM_Delta)
    detail_output.to_csv(out_doc_full_loc,sep=',',index=False)
    summary_output.to_csv(out_doc_summary_loc,sep=',',index=False)
    print("Skipped Symbols:", skipped_df.values)
    print('\007')
        # if stockopt_data.at[i, 'Expiry'] != stockopt_data.at[i+1, 'Expiry']:
        #     # print ("Expiry = " + stockopt_data.at[i, 'Expiry'])
    # print(stockopt_data.head(1))


def find_entry_strike():
    
    global prev_expiry_date
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global summary_output, sd_idx
    global ITM_Delta, OTM_Delta
    global symbol
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global reentry
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global corp_actions_df
    global from_units, to_units
    global default_count
    global trade_count
    
    trade_count = trade_count + 1
    entry_strike = (int(entry_stock_price + diff*step_length))
    if trail_cost == 0: # trailing stock price
        if cost > entry_stock_price:
            entry_strike = (int(cost + diff*step_length))
            # entry_stock_price = cost
    #     # else:
        #     entry_stock_price = int((entry_stock_price + cost)/2)
    
    # entry_strike = (int(cost/multiplier)+diff)*multiplier
    
    extra_amt = entry_strike % step_length
    entry_strike = entry_strike - extra_amt + reentry*step_length
    CE_sell_price_df_le = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']<=entry_strike))].reset_index()
    if CE_sell_price_df_le.empty == True:
        print (datetime.now(), "entry_date = ", entry_date, "expiry_date =", expiry_date, "Strike_price =",  entry_strike)
        CE_sell_price = 0
    else:
        CE_sell_price_df = CE_sell_price_df_le.iloc[[len(CE_sell_price_df_le) - 1]]
        entry_strike = int(CE_sell_price_df["Strike Price"])

        if CE_sell_price_df.empty == True:
            print (datetime.now(), 'expiry = ', expiry_date, ' entry on = ', entry_date, ' strike = ', entry_strike)
            print (datetime.now(), "Strike too far out of money.. trying for maximum strike available")    
            max_strike_df = stockopt_data[(stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date)]
            max_strike = max(max_strike_df["Strike Price"])
            entry_strike = max_strike
            print (datetime.now(), "Entering max strike of ", entry_strike)
            # CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reset_index()
            CE_sell_price_df_le = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']<=entry_strike))].reset_index()
            CE_sell_price_df = CE_sell_price_df_le.iloc[[len(CE_sell_price_df_le) - 1]]

            entry_strike = int(CE_sell_price_df["Strike Price"])
            # if CE_sell_price_df.empty == True:
            #     print ("couldn't locate maximum strike too - defaulting sell price to 0.05")
            #     CE_sell_price = 0.05
            # else:
            CE_sell_price = float(CE_sell_price_df["LTP"])
        else:
            CE_sell_price = float(CE_sell_price_df["LTP"])
    if CE_sell_price == 0:
        # print (datetime.now(), "strike not traded.. defaulting to intrinsic value..")
        # print (datetime.now(), "Entry date = ", entry_date)  
        default_count = default_count + 1
        if entry_strike > entry_stock_price:
            CE_sell_price = 0.1
        else:
            CE_sell_price = abs(entry_stock_price - entry_strike) + 0.1
        # print (datetime.now(), "Entry strike = ", entry_strike, "@ ", CE_sell_price)
    this_expiry_df = stockopt_data[(stockopt_data['Expiry']==expiry_date)].reset_index()

def calc_periods():

    global prev_expiry_date
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global summary_output, sd_idx
    global ITM_Delta, OTM_Delta
    global symbol
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global corp_actions_df
    global from_units, to_units

    entry_date_minus1 = datetime.strftime(datetime.strptime(entry_date,'%Y%m%d') - relativedelta(years=1),'%Y%m%d')
    year_data_df = historical_stock_prices[(historical_stock_prices['Date'] <= entry_date) & (historical_stock_prices['Date'] >= entry_date_minus1)]

    one_year_low = min(year_data_df['Low Price'])
    one_year_high = max(year_data_df['High Price'])
    one_year_midway = (one_year_high + one_year_low)/2
    one_year_qrtr = (one_year_low + one_year_midway)/2
    one_year_qrtr3 = (one_year_high + one_year_midway)/2
    
    if entry_stock_price < one_year_low:
        diff = base + 2*OTM_Delta
    elif entry_stock_price < one_year_qrtr:
        diff = base + OTM_Delta
    elif entry_stock_price < one_year_midway:
        diff = base 
    elif entry_stock_price < one_year_qrtr3:
        diff = base - ITM_Delta
    else:
        diff = base - 2*ITM_Delta 
    # if entry_stock_price > 1000:
    #     multiplier = 100
    # else:
    #     multiplier = 10
    # multiplier = 10

def load_files():
    
    global prev_expiry_date
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global summary_output, sd_idx
    global ITM_Delta, OTM_Delta
    global symbol
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global corp_actions_file
    global corp_actions_df
    global from_units, to_units
    global nifty_100_stock_list
    global nifty_100_stock_list_loc, nifty100

    in_doc_name = symbol + "_2014-01-01_2019-06-27.csv"
    historical_data = symbol + "_Historical_Data.csv"
                
    in_doc_full_loc = data_loc + in_doc_name
    out_doc_full_loc = data_loc + out_doc_name
    out_doc_summary_loc = data_loc + out_doc_summary
    historical_data_loc = data_loc + historical_data
    nifty_100_stock_list_loc = data_loc + nifty_100_stock_list
    # corp_action_path = data_loc + corp_actions_file

    stockopt_data = pd.read_csv(in_doc_full_loc,sep=',', parse_dates=['Date','Expiry'])
    historical_stock_prices = pd.read_csv(historical_data_loc,parse_dates=['Date'])
    
    # corp_actions_df = pd.read_csv(corp_action_path, parse_dates=['Ex-Date', 'Record Date'])
    # print('stockopt_data = ', stockopt_data)
    stockopt_data['Date'] = stockopt_data['Date'].dt.strftime('%Y%m%d')
    stockopt_data['Expiry'] = stockopt_data['Expiry'].dt.strftime('%Y%m%d')
    # step_size = int(stockopt_data[1,"Strike Price"]) - int(stockopt_data[0,"Strike Price"])
    # print('stockopt_data = ', stockopt_data)
    
    historical_stock_prices['Date'] = historical_stock_prices['Date'].dt.strftime('%Y%m%d')
    step_length = stockopt_data.at[1,"Strike Price"] - stockopt_data.at[0,"Strike Price"]

def expiry_change():
    global prev_expiry_date
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global summary_output, sd_idx
    global ITM_Delta, OTM_Delta
    global symbol
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global corp_actions_df
    global from_units, to_units, found_ca
    global default_count
    global trade_count

    # print(datetime.now(), " completed for expiry ", prev_expiry_date, " by trailing ", trailing)
    exit_date_df = stockopt_data[((stockopt_data['Date']==prev_exit_date) & (stockopt_data['Expiry']==prev_expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reset_index() 
    if exit_date_df.empty == True:
        print (datetime.now(), "probably a corporate action in month of expiry ", prev_exit_date,", defaulting to 0.5 for buy price and 0 for P/L, ", entry_strike, " strike not found")
        # check_corp_actions()
        # if found_ca == "Y":
        #     entry_strike = int(entry_strike*from_units/to_units)
        #     exit_date_df = stockopt_data[((stockopt_data['Date']==prev_exit_date) & (stockopt_data['Expiry']==prev_expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reset_index() 
        #     # CE_buy_price = 0.05
        #     exit_stock_price_df = stockopt_data[(stockopt_data['Date']==prev_exit_date) & (stockopt_data['Expiry']==prev_expiry_date)].reset_index()
        #     # print (exit_stock_price_df)
        #     exit_stock_price = float(exit_stock_price_df.at[1,'Stock Price'])
        #     # entry_strike = int(entry_strike*to_units/from_units)
        #     dd_idx = dd_idx + 1
        #     detail_output.loc[dd_idx] = [symbol, trailing, base, ITM_Delta, OTM_Delta, expiry_date, prev_exit_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price,0,0,0,one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high]
        # else:
        CE_buy_price = 0.05
        exit_stock_price_df = stockopt_data[(stockopt_data['Date']==prev_exit_date) & (stockopt_data['Expiry']==prev_expiry_date)].reset_index()
        # print (exit_stock_price_df)
        exit_stock_price = float(exit_stock_price_df.at[1,'Stock Price'])
        dd_idx = dd_idx + 1
        detail_output.loc[dd_idx] = [symbol, trailing, base, ITM_Delta, OTM_Delta, expiry_date, prev_exit_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price,0,0,0,one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high]
    else:
        CE_buy_price = float(exit_date_df["LTP"])
        exit_stock_price = float(exit_date_df["Stock Price"])
        dd_idx = dd_idx + 1
        detail_output.loc[dd_idx] = [symbol, trailing, base, ITM_Delta, OTM_Delta, expiry_date, prev_exit_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price,exit_stock_price-entry_stock_price, CE_sell_price-CE_buy_price,exit_stock_price-entry_stock_price+CE_sell_price-CE_buy_price,one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high]
    prev_expiry_date = expiry_date
    # dd_idx = dd_idx + 1
    # detail_output.loc[dd_idx] = [trailing, diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price,exit_stock_price-entry_stock_price, CE_sell_price-CE_buy_price,exit_stock_price-entry_stock_price+CE_sell_price-CE_buy_price,one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high]
    cost = (cost*expected_rate) - (CE_sell_price-CE_buy_price)
    # sd_idx = sd_idx + 1
    # summary_output.loc[sd_idx] = [trailing, base, prev_expiry_date]
    summary_output.at[sd_idx,'Trail/Cost'] = trailing
    summary_output.at[sd_idx,'Symbol'] = symbol
    summary_output.at[sd_idx,'Base'] = base
    summary_output.at[sd_idx,'Expiry'] = prev_expiry_date
    summary_output.at[sd_idx,'Trade Count'] = trade_count
    summary_output.at[sd_idx,'Not Traded Months'] = default_count
    summary_output.at[sd_idx,'ITM_Delta'] = ITM_Delta
    summary_output.at[sd_idx,'OTM_Delta'] = OTM_Delta
    summary_output.at[sd_idx,'P/L Stock'] += detail_output.loc[dd_idx]['P/L Stock']
    summary_output.at[sd_idx,'P/L Hedge'] += detail_output.loc[dd_idx]['P/L Hedge']
    summary_output.at[sd_idx,'Net P/L'] += detail_output.loc[dd_idx]['Net P/L']

def check_corp_actions():
    global prev_expiry_date
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global summary_output, sd_idx
    global ITM_Delta, OTM_Delta
    global symbol
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global corp_actions_df
    global from_units, to_units, found_ca
    
    corp_actions_df_temp = corp_actions_df[((corp_actions_df['Symbol']==symbol) & (corp_actions_df['Ex-Date']>=prev_expiry_date) & (corp_actions_df['Ex-Date']<=expiry_date))].reset_index()
    if corp_actions_df_temp.empty == True:
        found_ca = 'N'
    else:
        found_ca = 'Y'
        from_units = corp_actions_df_temp['From']
        to_units = corp_actions_df_temp['To']
        
def build_stock_list():

    global prev_expiry_date
    global symbol
    global prev_exit_date
    global entry_strike, entry_date
    global exit_date_df
    global CE_buy_price
    global exit_stock_price
    global detail_output, dd_idx
    global ITM_Delta, OTM_Delta
    global summary_output, sd_idx
    global base
    global cost
    global stockopt_data, historical_stock_prices
    global trailing, trail_cost
    global diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price
    global one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high
    global expected_rate
    global step_length
    global this_expiry_df
    global out_doc_summary_loc, out_doc_full_loc, data_loc, in_doc_name, out_doc_name, out_doc_summary, historical_data
    global reentry
    global corp_actions_file
    global corp_actions_df
    global from_units, to_units
    global stock_list
    global nifty_100_stock_list
    global nifty_100_stock_list_loc, nifty100

    entries  = os.scandir(data_loc)
    file_list_parsed = pd.DataFrame()
    total_stock_list = pd.DataFrame()
    
    for i in entries:
        if i.name.find("2014-01-01_2019-06-27.csv") > 0:
            parsed_name = i.name.split("_")
            file_list_parsed = file_list_parsed.append(pd.DataFrame(parsed_name).transpose())
    # final_df = pd.DataFrame(file_list_parsed, columns=['Instrument Type', 'Symbol','Option type','From Date','To String','To Date'])
    file_list_parsed.columns = ['Symbol','From_Date','To_Date']
    # To_Date_temp = file_list_parsed['To_Date']
    total_stock_list = pd.DataFrame(file_list_parsed['Symbol'])
    total_stock_list.columns = ['Symbol']
    print (total_stock_list)
    nifty_100_stock_list_loc = data_loc + nifty_100_stock_list
    nifty100 = pd.read_csv(nifty_100_stock_list_loc)
    nifty100.columns=['Company Name','Industry','Symbol','Series', 'ISIN Code']
    print (nifty100)
    stock_list = nifty100.merge(total_stock_list, on='Symbol')
    print (stock_list)    
    # stock_list.columns = ['Symbol','Company Name','Industry','Series', 'ISIN Code']

if __name__ == "__main__":
    
    # main()
    try:
        main()
    except:
        # print('\007')
        os.system("Error.mp3") 
        # sys.exit()
    else:
        os.system("Success.mp3")
        
 
  