# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
import os
import shutil
import datetime


def main():
    global input_path
    global out_path
    global Output_Data
    global csv_data

    input_path = 'C:/Ramu/Investment_Strategies/Katalon/All_Data_Merged/'
    out_path = 'C:/Ramu/Investment_Strategies/Katalon/All_Data_Merged/'
    
    select_files()

    print ('Completed..')


def select_files():

    global input_path
    global csv_data
    entries  = os.scandir(input_path)
    
    for i in entries:
        # print (i.name)
        if i.name.find('01-01-2014_31-12-2018') >= 0 and i.name.find('Exceptions') < 0:
            outdf = pd.DataFrame()
            # print('Processing file ', i.name)
            outfile = out_path + 'Exceptions_' + i.name
            input_file = input_path + i.name
            inputdf = pd.read_csv(input_file,sep=',',parse_dates=['Date', 'Expiry'])
            inputdf['Date'] = inputdf['Date'].dt.strftime('%Y%m%d')
            inputdf['Expiry'] = inputdf['Expiry'].dt.strftime('%Y%m%d')
            expiry_dates = inputdf['Expiry'].unique()
            for j in range(0,len(expiry_dates)):
                if inputdf[(inputdf['Date'] == expiry_dates[j])]['Date'].empty == True:
                    missing_day = datetime.datetime.strptime(expiry_dates[j],'%Y%m%d')
                    print ('File = ', i.name, ' Missing Entry date for expiry ', missing_day.date())
                    next_day = missing_day + datetime.timedelta(days= 7-missing_day.weekday() if missing_day.weekday()>3 else 1)
                    print ('change expiry to next business day', datetime.datetime.strftime(next_day,'%Y%m%d'))
                    # # inputdf.is_copy = False
                    # outdf = inputdf[(inputdf['Expiry']==expiry_dates[j])]['Expiry']
                    # outdf.loc['Expiry'] = datetime.datetime.strftime(next_day,'%Y%m%d')
                    # outdf.to_csv(outfile,sep=',')

if __name__ == "__main__":
  main()
  
  