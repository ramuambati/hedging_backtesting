import pandas as pd
import numpy as np
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta, FR, TH 

def main():
    data_loc = "C:/Ramu/Investment_Strategies/Katalon/All_Data_Merged/"
    in_doc_name = "RELIANCE_01-01-2014_31-12-2018.csv"
    out_doc_name = "RELIANCE_01-01-2014_31-12-2018_Detail.csv"
    out_doc_summary = "RELIANCE_01-01-2014_31-12-2018_Summary.csv"
    historical_data = "RELIANCE_Historical_Data.csv"
    expected_rate = 1.0
    
    in_doc_full_loc = data_loc + in_doc_name
    out_doc_full_loc = data_loc + out_doc_name
    out_doc_summary_loc = data_loc + out_doc_summary
    historical_data_loc = data_loc + historical_data

    stockopt_data = pd.read_csv(in_doc_full_loc,sep=',', parse_dates=['Date','Expiry'])
    historical_stock_prices = pd.read_csv(historical_data_loc,parse_dates=['Date'])
    # print('stockopt_data = ', stockopt_data)
    stockopt_data['Date'] = stockopt_data['Date'].dt.strftime('%Y%m%d')
    stockopt_data['Expiry'] = stockopt_data['Expiry'].dt.strftime('%Y%m%d')
    # print('stockopt_data = ', stockopt_data)
    
    historical_stock_prices['Date'] = historical_stock_prices['Date'].dt.strftime('%Y%m%d')

    detail_output = pd.DataFrame(columns=['Trail/Cost','diff', 'Expiry','Stock Price Entry', 'Stock Price Exit', 'Strike Price', 'Cost','Opt Sell price','Opt Buy price','P/L Stock','P/L Hedge','Net P/L',' TTM low','TTM 25%','TTM 50%','TTM 75%','TTM high'])
    summary_output = pd.DataFrame(columns=['Trail/Cost','diff', 'Expiry', 'P/L Stock','P/L Hedge','Net P/L'])
    dd_idx = 0
    sd_idx = 0

    # trail_cost = 0 means selling near cost of entry, adjusted for booked profit
    # trail_cost = 1 means selling near stock price
    for trail_cost in range (0,2):
        # cost = entry_stock_price
        if trail_cost == 1:
            trailing = 'Stock'
        else:
            trailing = "Cost"
        # for diff in range(-2,2):
        for diff_temp in range (0,1):
            # diff = -1
            # diff = 1 for nearest OTM strike
            # diff = 0 for nearest ITM strike
            # diff = -1/-2 for deep ITM strike


            entry_date = stockopt_data.at[1,"Date"]
            entry_stock_price = stockopt_data.at[1,"Stock Price"]
            expiry_date = stockopt_data.at[1,"Expiry"]
            # temp_date = datetime.strptime(entry_date,'%d-%b-%y').strftime('%Y%m%d')
            # temp_date2 = datetime.strptime(temp_date,'%Y',mmdd)
            # temp_date2 = temp_date2-1
            # mm_dd = datetime.strptime(temp_date,'%d-%b')
            entry_date_minus1 = datetime.strftime(datetime.strptime(entry_date,'%Y%m%d') - relativedelta(years=1),'%Y%m%d')
            year_data_df = historical_stock_prices[(historical_stock_prices['Date'] <= entry_date) & (historical_stock_prices['Date'] >= entry_date_minus1)]

            one_year_low = min(year_data_df['Low Price'])
            one_year_high = max(year_data_df['High Price'])
            one_year_midway = (one_year_high + one_year_low)/2
            one_year_qrtr = (one_year_low + one_year_midway)/2
            one_year_qrtr3 = (one_year_high + one_year_midway)/2
            
            print ('entry date = ', entry_date)
            print ('entry stock price = ', entry_stock_price)
            print ('expiry_date = ', expiry_date)
            print ('entry_date_minus1 = ', entry_date_minus1)
            # print ('year_data_df = ', year_data_df )

            if entry_stock_price < one_year_low:
                diff = 2
            elif entry_stock_price < one_year_qrtr:
                diff = 2
            elif entry_stock_price < one_year_midway:
                diff = 1
            elif entry_stock_price < one_year_qrtr3:
                diff = 0
            else:
                diff = -1
            if entry_stock_price > 1000:
                multiplier = 100
            else:
                multiplier = 10
               
            if trail_cost == 1: # trailing stock price
                entry_strike = (int(entry_stock_price/multiplier)+diff)*multiplier
                cost = entry_stock_price
            else:
                cost = entry_stock_price
                print ("cost at entry = ", cost)
                entry_strike = (int(cost/multiplier)+diff)*multiplier

            CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reindex()
            if CE_sell_price_df.empty == True:
                print ('expiry = ', expiry_date, ' entry on = ', entry_date, ' strike = ', entry_strike)
                print ("Strike too far out of money.. trying for maximum strike available")    
                max_strike_df = stockopt_data[(stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date)]
                max_strike = max(max_strike_df["Strike Price"])
                entry_strike = (int(max_strike/multiplier))*multiplier
                print ("Entering max strike of ", entry_strike)
                CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reindex()
                if CE_sell_price_df.empty == True:
                    print ("couldn't locate maximum strike too - defaulting sell price to 0.05")
                    CE_sell_price = 0.05
                else:
                    CE_sell_price = float(CE_sell_price_df["LTP"])
            else:
                CE_sell_price = float(CE_sell_price_df["LTP"])
            if CE_sell_price == 0:
                print ("strike not traded.. defaulting to intrinsic value..")
                print ("Entry date = ", entry_date)  
                if entry_strike > entry_stock_price:
                    CE_sell_price = 0.1
                else:
                    CE_sell_price = abs(entry_stock_price - entry_strike) + 0.1
                print ("Entry strike = ", entry_strike, "@ ", CE_sell_price, "\n\n\n")
            prev_expiry_date = expiry_date
            sd_idx = sd_idx + 1
            summary_output.loc[sd_idx] = [trailing, diff, expiry_date,0,0,0]
            # print ("Trailing ", trailing)
            # print ("diff = ", diff)
            # print ("Entry date = ", entry_date)
            # print ("Stock Price @ entry = ", entry_stock_price)
            # print ("Expiry Date = ", expiry_date)
            # print ("Entry strike = ", entry_strike, "@ ", CE_sell_price, "\n\n\n")
            
            for i in range(0, len(stockopt_data)-1):
                if ((stockopt_data.at[i, 'Date'] == expiry_date) and (stockopt_data.at[i,'Expiry'] == expiry_date) and (stockopt_data.at[i,'Strike Price']==entry_strike)):
                    CE_buy_price = float(stockopt_data.at[i,"LTP"])
                    exit_stock_price = float(stockopt_data.at[i,'Stock Price'])
                    prev_expiry_date = expiry_date
                    dd_idx = dd_idx + 1
                    detail_output.loc[dd_idx] = [trailing, diff, expiry_date, entry_stock_price, exit_stock_price, entry_strike, cost, CE_sell_price, CE_buy_price,exit_stock_price-entry_stock_price, CE_sell_price-CE_buy_price,exit_stock_price-entry_stock_price+CE_sell_price-CE_buy_price,one_year_low,one_year_qrtr, one_year_midway, one_year_qrtr3, one_year_high]
                    cost = (cost*expected_rate) - (CE_sell_price-CE_buy_price)
                    summary_output.loc[sd_idx]['P/L Stock'] += detail_output.loc[dd_idx]['P/L Stock']
                    summary_output.loc[sd_idx]['P/L Hedge'] += detail_output.loc[dd_idx]['P/L Hedge']
                    summary_output.loc[sd_idx]['Net P/L'] += detail_output.loc[dd_idx]['Net P/L']
                                    
                    # print ("detail_output = ", detail_output)

                if (stockopt_data.at[i,"Expiry"] != prev_expiry_date):
                    entry_date = prev_expiry_date
                    entry_stock_price = exit_stock_price
                    expiry_date = stockopt_data.at[i,"Expiry"]
                    
                    entry_date_minus1 = datetime.strftime(datetime.strptime(entry_date,'%Y%m%d') - relativedelta(years=1),'%Y%m%d')
                    year_data_df = historical_stock_prices[(historical_stock_prices['Date'] <= entry_date) & (historical_stock_prices['Date'] >= entry_date_minus1)]
                    one_year_low = min(year_data_df['Low Price'])
                    one_year_high = max(year_data_df['High Price'])
                    one_year_midway = (one_year_high + one_year_low)/2
                    one_year_qrtr = (one_year_low + one_year_midway)/2
                    one_year_qrtr3 = (one_year_high + one_year_midway)/2
                    if entry_stock_price <= one_year_low:
                        diff = 2
                    elif entry_stock_price <= one_year_qrtr:
                        diff = 2
                    elif entry_stock_price <= one_year_midway:
                        diff = 1
                    elif entry_stock_price <= one_year_qrtr3:
                        diff = 0
                    else:
                        diff = -1

                    if entry_stock_price > 1000:
                        multiplier = 100
                    else:
                        multiplier = 10
                    if trail_cost == 1: # trailing stock price
                        entry_strike = (int(entry_stock_price/multiplier)+diff)*multiplier
                    else: # trailing cost price
                        if cost > entry_stock_price:
                            # entry_strike = (int(cost/10)+diff)*10
                            entry_strike = (int((entry_stock_price + 2*cost)/(3*multiplier))+diff)*multiplier
                        else:
                            entry_strike = (int(entry_stock_price/multiplier)+diff)*multiplier
                    # entry_strike = (int(entry_stock_price/10)+diff)*10
                    CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reindex()
                    # if CE_sell_price_df.empty == True:
                    #     entry_strike = entry_strike-10
                    #     CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reindex()
                    if CE_sell_price_df.empty == True:
                        print ('expiry = ', expiry_date, ' entry on = ', entry_date, ' strike = ', entry_strike)
                        print ("Strike too far out of money.. trying for maximum strike available")    
                        max_strike_df = stockopt_data[(stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date)]
                        max_strike = max(max_strike_df["Strike Price"])
                        entry_strike = (int(max_strike/multiplier))*multiplier
                        print ("Entering max strike of ", entry_strike)
                        CE_sell_price_df = stockopt_data[((stockopt_data['Date']==entry_date) & (stockopt_data['Expiry']==expiry_date) & (stockopt_data['Strike Price']==entry_strike))].reindex()
                        if CE_sell_price_df.empty == True:
                            print ("couldn't locate maximum strike too - defaulting sell price to 0.05")
                            CE_sell_price = 0.05
                        else:
                            CE_sell_price = float(CE_sell_price_df["LTP"]) 
                    else:
                        CE_sell_price = float(CE_sell_price_df["LTP"])
                    
                    if CE_sell_price == 0:
                        print ("strike not traded.. defaulting to intrinsic value..")
                        print ("Entry date = ", entry_date)  
                        if entry_strike > entry_stock_price:
                            CE_sell_price = 0.1
                        else:
                            CE_sell_price = abs(entry_stock_price - entry_strike) + 0.1
                        print ("Entry strike = ", entry_strike, "@ ", CE_sell_price, "\n\n\n")
                    # print ("Entry date = ", entry_date)
                    # print ("Stock Price @ entry = ", entry_stock_price)
                    # print ("Expiry Date = ", expiry_date)
                    # print ("Entry strike = ", entry_strike, "@ ", CE_sell_price, "\n\n\n")
                    prev_expiry_date = expiry_date

                if  summary_output.loc[sd_idx]['diff'] != diff:
                    sd_idx = sd_idx + 1
                    summary_output.loc[sd_idx] = [trailing, diff, expiry_date,0,0,0]
                    
    detail_output.to_csv(out_doc_full_loc,sep=',',index=False)
    summary_output.to_csv(out_doc_summary_loc,sep=',',index=False)
        # if stockopt_data.at[i, 'Expiry'] != stockopt_data.at[i+1, 'Expiry']:
        #     # print ("Expiry = " + stockopt_data.at[i, 'Expiry'])
    # print(stockopt_data.head(1))


if __name__ == "__main__":
  main()