# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
import os
import shutil
import datetime as dt

def main():
    global input_path
    global out_path
    global Output_Data
    global file_list_parsed
    global entries
    global stocks_included
    global stocks_skipped
    # global historical_data_path
    print (dt.datetime.now(), 'Processing started..')
    input_path = 'C:/Stocks_Data/Hedging_Analysis/Downloaded_Data/'
    # historical_data_path = 'C:/Ramu/Investment_Strategies/Katalon/Historical_Data/'
    out_path = 'C:/Stocks_Data/Hedging_Analysis/Downloaded_Data_Merged/'
    
    stocks_skipped = pd.DataFrame()
    load_file_names()
    # find_stocks()
    Merge_files()

    print(stocks_skipped)
    print (dt.datetime.now(),'Completed..')
    print('\007')


def load_file_names():

    global input_path
    global file_list_parsed
    global entries
    global stocks_included

    entries  = os.scandir(input_path)
    file_list_parsed = pd.DataFrame()
    
    for i in entries:
        if i.name.find(".csv") > 0:
            parsed_name = i.name.split("_")
            # print (dt.datetime.now(), parsed_name)
            # Symbol = parsed_name[1]
            # From_Date = parsed_name[3]
            # To_Date_temp = parsed_name[5]
            # print (To_Date_temp)
            # To_Date = To_Date_temp[:-4]
            # print (To_Date)
            # print (dt.datetime.now(), parsed_name)
            file_list_parsed = file_list_parsed.append(pd.DataFrame(parsed_name).transpose())
    # final_df = pd.DataFrame(file_list_parsed, columns=['Instrument Type', 'Symbol','Option type','From Date','To String','To Date'])
    file_list_parsed.columns = ['Instrument_Type', 'Symbol','Option_type','From_Date','To_String','To_Date']
    # To_Date_temp = file_list_parsed['To_Date']
    file_list_parsed['To_Date'] = file_list_parsed['To_Date'].str.split(".", n = 1, expand = True) 
    print (file_list_parsed)
    file_list_parsed['From_Date'] = pd.to_datetime(file_list_parsed['From_Date'].astype(str))
    
    file_list_parsed['To_Date'] = pd.to_datetime(file_list_parsed['To_Date'].astype(str))
    print (dt.datetime.now(), "total files parsed = ", len(file_list_parsed))
    print (file_list_parsed)

def Merge_files():

    global input_path
    global file_list_parsed
    global entries
    global out_path
    global stocks_included
    global stocks_skipped
    # global historical_data_path

    stocks_included = file_list_parsed['Symbol'].unique()
    # stocks_included = ['RELIANCE']
    # print (dt.datetime.now(), stocks_included)
    for j in range(0,len(stocks_included)):
        stock = stocks_included[j]
        outdf = pd.DataFrame()
        print (dt.datetime.now(), 'Processing ', stock)
        hist_file_name = out_path + stock + '_Historical_Data.csv'
        if os.path.exists(hist_file_name) == True:
            hist_df = pd.read_csv(hist_file_name,sep=',',parse_dates=['Date'])
            hist_df = hist_df.loc[hist_df["Series"] == "EQ"]
            hist_df['Date'] = hist_df['Date'].dt.strftime('%Y%m%d')
            entries  = os.scandir(input_path)
            for i in entries:
                # print('in entries', i)
                # string_to_find = 'OPTSTK_'+stock+'_'
                # print(string_to_find)
                if i.name.find('OPTSTK_'+stock+'_') >= 0:
                    infile_name = input_path + i.name
                    # print(infile_name)
                    inputdf = pd.read_csv(infile_name,sep=',',parse_dates=['Date','Expiry'])
                    inputdf['Date'] = inputdf['Date'].dt.strftime('%Y%m%d')
                    inputdf['Expiry'] = inputdf['Expiry'].dt.strftime('%Y%m%d')
                    merged_df = inputdf.merge(hist_df,on="Date")
                    # print(inputdf)
                    outdf = outdf.append(merged_df)
            # print(outdf)
            outdf = outdf.rename(index=str, columns={"Symbol_x":"Symbol", "Last Price":"Stock Price"})
            outdf.drop(columns=['Underlying Value', 'Option Type', 'Premium Turnover in Lacs', 'Symbol_y', '% Dly Qt to Traded Qty'])
            # print (outdf)
            outdf = outdf.sort_values(['Symbol', 'Expiry', 'Date', 'Strike Price'], ascending=True)
            outfile_name = stock + '_' + str(min(file_list_parsed['From_Date']))[:10] + '_' + str(max(file_list_parsed['To_Date']))[:10] + '.csv'
            outfile = out_path + outfile_name
            # Fix to address general election holiday in 2014
            # outdf.loc[outdf['Expiry'] == '20140424','Expiry'] = '20140423'
            outdf.loc[outdf['Date'] == '20140425','Date'] = '20140424'
            # Fix to change in name of Sterlite to Vedanta
            if stock == 'VEDL':
                outdf.loc[outdf['Symbol'] == 'SSLT','Symbol'] = 'VEDL'
                
            outdf.to_csv(outfile, sep=',', index=False)
            print (dt.datetime.now(), 'output saved at ', outfile)
        else:
            print ("Skipped ", stock, "as historical stock info does not exist")
            stocks_skipped = stocks_skipped.append({"Skipped Stocks" : stock},ignore_index=True)

def find_stocks():

    global input_path
    global file_list_parsed
    global entries
    global out_path
    global stocks_included
    
    
    stocks_included = pd.DataFrame(columns=["Symbol"])
    entries  = os.scandir(input_path)
    for i in entries:
        if i.name.find(".csv") > 0:
            parsed_name_list = i.name.split('_')
            parsed_name = parsed_name_list[1]
            # print (parsed_name)
            stocks_included = stocks_included.append({"Symbol" : parsed_name},ignore_index=True)
    
    stocks_included = stocks_included.drop_duplicates().reset_index()
    print (dt.datetime.now(), "number of stocks = ", len(stocks_included))
    # print (dt.datetime.now(), "stocks = ", stocks_included)
if __name__ == "__main__":
  main()
  
  