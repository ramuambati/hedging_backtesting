# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np
import os
import shutil

def main():
    global input_path
    global out_path
    global Output_Data
    global csv_data

    input_path = 'C:/Stocks_Data/Hedging_Analysis/Downloaded_Data_Merged/NIFTY50/'
    
    # file_name = input_path + 'FUTIDX_NIFTY50_01-01-2014_28-06-2019.csv'
    # outdf = pd.read_csv(file_name,sep=',',parse_dates=['Date','Expiry'])
    # outdf = outdf.sort_values(['Symbol', 'Expiry', 'Date'], ascending=True)
    # outdf.to_csv(file_name, sep=',', index=False)

    # file_name = input_path + 'NIFTY50_Historical_Data.csv'
    # outdf = pd.read_csv(file_name,sep=',',parse_dates=['Date'])
    # outdf = outdf.sort_values(['Date'], ascending=True)
    # outdf.to_csv(file_name, sep=',', index=False)

    file_name = input_path + 'OPTIDX_NIFTY50_CE_01-01-2014_28-06-2019.csv'
    outdf = pd.read_csv(file_name,sep=',',parse_dates=['Date','Expiry'])
    outdf = outdf.sort_values(['Symbol', 'Expiry', 'Date', 'Strike Price'], ascending=True)
    outdf.to_csv(file_name, sep=',', index=False)

    # file_name = input_path + 'OPTIDX_NIFTY50_PE_01-01-2014_28-06-2019.csv'
    # outdf = pd.read_csv(file_name,sep=',',parse_dates=['Date','Expiry'])
    # outdf = outdf.sort_values(['Symbol', 'Expiry', 'Date', 'Strike Price'], ascending=True)
    # outdf.to_csv(file_name, sep=',', index=False)

    print ('Completed..')




   
if __name__ == "__main__":
  main()
  
  