import pandas as pd
import numpy as np
import os
import sys
import shutil
from datetime import datetime
from datetime import timedelta
from dateutil.relativedelta import relativedelta, FR, TH 

global data_loc, Options_File_Name, Future_File_Name
global Future_df, Options_df, step_length, diff, reentry
global entry_date, expiry_date, PE_sell_price, this_expiry_df, entry_strike
global detail_output, summary_output, sd_idx, dd_idx
global prev_exit_date, prev_expiry_date, PE_buy_price
global base, ITM_Delta, OTM_Delta, entry_nifty_value, trade_count, default_count, Fut_sell_price
global trade_count, default_count
global exit_nifty_value

def main():
 
    global data_loc, Options_File_Name, Future_File_Name
    global Future_df, Options_df, step_length, diff, reentry
    global entry_date, expiry_date, PE_sell_price, this_expiry_df, entry_strike
    global prev_exit_date, prev_expiry_date, PE_buy_price
    global detail_output, summary_output, sd_idx, dd_idx
    global base, ITM_Delta, OTM_Delta, entry_nifty_value, trade_count, default_count, Fut_sell_price
    global trade_count, default_count
    global exit_nifty_value

    dd_idx = 0
    sd_idx = 1

    detail_output = pd.DataFrame(columns=['Symbol', 'Base', 'ITM_Delta', 'OTM_Delta', 'Expiry', 'Date', 'Nifty @ Entry', 'Nifty @ Exit', 'Strike Price', 'Fut Sell Price', 'Fut Buy Price', 'Opt Sell price','Opt Buy price','P/L Nifty','P/L Hedge','Net P/L'])
    summary_output = pd.DataFrame(columns=['Symbol', 'Base', 'ITM_Delta', 'OTM_Delta', 'Expiry', 'Nifty @ Entry', 'Nifty @ Exit', 'P/L Nifty','P/L Hedge','Net P/L'])
    
      
    data_loc = "C:/Stocks_Data/Hedging_Analysis/Downloaded_Data_Merged/NIFTY50/"
    out_doc_name = data_loc + "NIFTY50_SFSP_2014-01-01_2019-06-27_Detail_V4.csv"
    out_doc_summary = data_loc + "NIFTY50_SFSP_2014-01-01_2019-06-27_Summary_V4.csv"

    Future_File_Name = 'FUTIDX_NIFTY50_01-01-2014_28-06-2019.csv'
    Options_File_Name = 'OPTIDX_NIFTY50_PE_01-01-2014_28-06-2019.csv'
    
    symbol = 'NIFTY50'
    print('/007')
    print (datetime.now(), "- Processing started for ", symbol)
    

    expected_rate = 1.0
    base = 1        # what entity to pick when it's around 50% TTM
    ITM_Delta = 1   # What entity to pick when it's above 50% TTM
    OTM_Delta = 1   # What entity to pick when it's near TTM low

    load_files()

    for base in range(0,1):
        for ITM_Delta in range(0,1):
            for OTM_Delta in range(3,6):
                default_count = 0
                trade_count = 0
                summary_output.at[sd_idx,'P/L Nifty'] = 0
                summary_output.at[sd_idx,'P/L Hedge'] = 0
                summary_output.at[sd_idx,'Net P/L'] = 0
                summary_output.at[sd_idx,'Nifty @ Entry'] = Future_df.at[0,'Stock Price']

                entry_date = Future_df.at[0,"Date"]
                entry_nifty_value = Future_df.at[0,'Stock Price']
                expiry_date = Future_df.at[0,"Expiry"]
                
                diff = base + ITM_Delta - OTM_Delta

                reentry = 1

                Fut_sell_df = Future_df[((Future_df['Date']==entry_date) & (Future_df['Expiry']==expiry_date))].reset_index()
                # print ('entry_date = ', entry_date, 'expiry_date = ', expiry_date)
                # print(Fut_sell_df)
                Fut_sell_price = Fut_sell_df.at[0, 'LTP']

                find_entry_strike()

                prev_expiry_date = expiry_date
                exit_date = Options_df.at[0, 'Date']

                for i in range(0, len(Options_df)-1):

                    prev_exit_date = exit_date
                    exit_date = Options_df.at[i, 'Date']

                    if (Options_df.at[i,"Expiry"] != prev_expiry_date):
                        
                        expiry_change()
                        entry_date = prev_expiry_date
                        entry_nifty_value = exit_nifty_value
                        expiry_date = Options_df.at[i,"Expiry"]
                        
                        reentry = 1
                        Fut_sell_df = Future_df[((Future_df['Date']==entry_date) & (Future_df['Expiry']==expiry_date))].reset_index()
                        # print ('entry_date = ', entry_date, 'expiry_date = ', expiry_date)
                        # print(Fut_sell_df)
                        Fut_sell_price = Fut_sell_df.at[0, 'LTP']
                        find_entry_strike()
                        prev_expiry_date = expiry_date
                    
                    # target_up = entry_strike + step_length

                    # if (Options_df.at[i,"Stock Price"] >= target_up):
                    #     prev_exit_date = exit_date
                    #     expiry_change()
                    #     entry_date = prev_exit_date
                    #     entry_nifty_value = exit_nifty_value
                    #     expiry_date = Options_df.at[i,"Expiry"]
                        
                    #     reentry = 1
                    #     find_entry_strike()
                    #     prev_expiry_date = expiry_date

                expiry_change()
                sd_idx = sd_idx + 1
                print(datetime.now(), symbol, " completed for base ", base, "for ITM Delta of ", ITM_Delta, " and OTM Delta of ", OTM_Delta)
    detail_output.to_csv(out_doc_name,sep=',',index=False)
    summary_output.to_csv(out_doc_summary,sep=',',index=False)
    print('/007')


def find_entry_strike():
    
    global Future_df, Options_df, step_length, diff, reentry
    global entry_date, expiry_date, PE_sell_price, this_expiry_df, PE_sell_price, entry_strike
    global trade_count, default_count
    global entry_nifty_value
    
    trade_count = trade_count + 1
    entry_strike = int(entry_nifty_value - diff*step_length)
    
    extra_amt = entry_strike % step_length
    # entry_strike = entry_strike - extra_amt + reentry*step_length
    entry_strike = entry_strike - extra_amt 

    Opt_sell_df = Options_df[((Options_df['Date']==entry_date) & (Options_df['Expiry']==expiry_date) & (Options_df['Strike Price']<=entry_strike))].reset_index()
    
    if Opt_sell_df.empty == True:
        print (datetime.now(), "entry_date = ", entry_date, "expiry_date =", expiry_date, "Strike_price =",  entry_strike)
        PE_sell_price = 0
    else:
        Opt_sell_df = Opt_sell_df.iloc[[len(Opt_sell_df) - 1]]
        entry_strike = int(Opt_sell_df["Strike Price"])

        if Opt_sell_df.empty == True:
            print (datetime.now(), 'expiry = ', expiry_date, ' entry on = ', entry_date, ' strike = ', entry_strike)
            print (datetime.now(), "Strike too far out of money.. trying for maximum strike available")    
            max_strike_df = Options_df[(Options_df['Date']==entry_date) & (Options_df['Expiry']==expiry_date)]
            max_strike = max(max_strike_df["Strike Price"])
            entry_strike = max_strike
            print (datetime.now(), "Entering max strike of ", entry_strike)
            Opt_sell_df = Options_df[((Options_df['Date']==entry_date) & (Options_df['Expiry']==expiry_date) & (Options_df['Strike Price']<=entry_strike))].reset_index()
            Opt_sell_df = Opt_sell_df.iloc[[len(Opt_sell_df) - 1]]
            entry_strike = Opt_sell_df["Strike Price"]
            PE_sell_price = float(Opt_sell_df["LTP"])
        else:
            PE_sell_price = float(Opt_sell_df["LTP"])
    # print(PE_sell_price)
    if PE_sell_price == 0:
        default_count = default_count + 1
        if entry_strike > entry_nifty_value:
            PE_sell_price = 0.1
        else:
            PE_sell_price = abs(entry_nifty_value - entry_strike) + 0.1
    this_expiry_df = Options_df[(Options_df['Expiry']==expiry_date)].reset_index()

def load_files():
    
    global data_loc, Options_File_Name, Future_File_Name
    global Future_df, Options_df, step_length
    global exit_nifty_value

    FFN_full_loc = data_loc + Future_File_Name
    OFN_full_loc = data_loc + Options_File_Name

    Future_df = pd.read_csv(FFN_full_loc,sep=',', parse_dates=['Date','Expiry'])
    Options_df = pd.read_csv(OFN_full_loc,sep=',', parse_dates=['Date', 'Expiry'])
    
    
    step_length = 2*(Options_df.at[1,"Strike Price"] - Options_df.at[0,"Strike Price"])

def expiry_change():
    global Future_df, Options_df, step_length, diff, reentry
    global entry_date, expiry_date, PE_sell_price, this_expiry_df, entry_strike
    global prev_exit_date, prev_expiry_date, PE_buy_price
    global detail_output, summary_output, sd_idx, dd_idx
    global base, ITM_Delta, OTM_Delta, entry_nifty_value, trade_count, default_count, Fut_sell_price
    global exit_nifty_value

    exit_nifty_value_df = Future_df[((Future_df['Date']==prev_exit_date) & (Future_df['Expiry']==prev_expiry_date))]
    exit_nifty_value = exit_nifty_value_df['Stock Price']

    Fut_buy_price = exit_nifty_value_df['LTP']

    exit_PE_df = Options_df[((Options_df['Date']==prev_exit_date) & (Options_df['Expiry']==prev_expiry_date) & (Options_df['Strike Price']==entry_strike))].reset_index() 

    if exit_PE_df.empty == True:
        print (datetime.now(), "probably a corporate action in month of expiry ", prev_exit_date,", defaulting to 0.5 for buy price and 0 for P/L, ", entry_strike, " strike not found")
        PE_buy_price = 0.05
        dd_idx = dd_idx + 1
        detail_output.loc[dd_idx] = ['NIFTY50', base, ITM_Delta, OTM_Delta, expiry_date, prev_exit_date, float(entry_nifty_value), float(exit_nifty_value), entry_strike, float(Fut_sell_price), float(Fut_buy_price), float(PE_sell_price), float(PE_buy_price) ,0,0,0]
    else:
        PE_buy_price = exit_PE_df["LTP"]
        exit_stock_price = exit_PE_df["Stock Price"]
        dd_idx = dd_idx + 1
        detail_output.loc[dd_idx] = ['NIFTY50', base, ITM_Delta, OTM_Delta, expiry_date, prev_exit_date, float(entry_nifty_value), float(exit_nifty_value), entry_strike, float(Fut_sell_price), float(Fut_buy_price), float(PE_sell_price), float(PE_buy_price), float(Fut_sell_price)-float(Fut_buy_price), float(PE_sell_price)-float(PE_buy_price),float(Fut_sell_price)-float(Fut_buy_price)+float(PE_sell_price)-float(PE_buy_price)]

    prev_expiry_date = expiry_date
    
    summary_output.at[sd_idx,'Symbol'] = 'NIFTY50'
    summary_output.at[sd_idx,'Base'] = base
    summary_output.at[sd_idx,'Expiry'] = prev_expiry_date
    summary_output.at[sd_idx,'Trade Count'] = trade_count
    summary_output.at[sd_idx,'Not Traded Months'] = default_count
    summary_output.at[sd_idx,'ITM_Delta'] = ITM_Delta
    summary_output.at[sd_idx,'OTM_Delta'] = OTM_Delta
    summary_output.at[sd_idx,'P/L Nifty'] += detail_output.loc[dd_idx]['P/L Nifty']
    summary_output.at[sd_idx,'P/L Hedge'] += detail_output.loc[dd_idx]['P/L Hedge']
    summary_output.at[sd_idx,'Net P/L'] += detail_output.loc[dd_idx]['Net P/L']

if __name__ == "__main__":
    
    main()
    # try:
    #     main()
    # except:
    #     # print('/007')
    #     os.system("Error.mp3") 
    #     # sys.exit()
    # else:
    #     os.system("Success.mp3")
        
 
  