# 
# Run through various strategies for derivatives
#

import pandas as pd
import numpy as np

def main():

# Defining Global Variables..
    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global max_rows
    global result_array
    global result_array_hdr
    global result_array_col
    global LTQ

# URL Location and Reading HTML..

    url = 'C:/Users/ambatir/Desktop/Bank_Nifty_HTML.html'
    # url = 'https://www.nseindia.com/live_market/dynaContent/live_watch/option_chain/optionKeys.jsp?segmentLink=17&instrument=OPTIDX&symbol=BANKNIFTY&date=23AUG2018'
    tables = pd.read_html(url)
    i = 0

# Identify the underlying Index and Current Price
    itable = tables[0]
    idf = pd.DataFrame(data=itable)
    index_array = idf.values
    
    max_rows = idf.shape[0]
    max_cols = idf.shape[1]

    row = 0
    col = 0
    for row in range(max_rows):
        for col in range(max_cols):
            print (row, col, index_array[row, col])

    array_words = index_array[0, 1].split()
    Index = array_words[2]
    LTP = float(array_words[3])
    if Index == "NIFTY":
        LTQ = 75
    elif Index == "BANKNIFTY":
        LTQ = 40
    else:
        LTQ = 1

    print("Index = ", Index)
    print("LTP = ", LTP)

# Identify the Option Chain Table - It is mostly in 2nd table but some times also in 3rd.
    octable = tables[1]
        
    # print (tables[1])
    df = pd.DataFrame(data=octable)
    oc_array = df.values
    
    max_rows = df.shape[0]
    max_cols = df.shape[1]
    result_array = np.zeros((max_rows-1,7))
    result_array_hdr = np.full((7,7),"                       ")
        
    # print (result_array)


    print ("max_cols = ", max_cols)
    print ("max_rows = ", max_rows)
    row = 0
    col = 11
    
# Find the Current Strike Price in Option Chain Table
    for row in range(max_rows):
        if (LTP < float(oc_array[row, col])):
            spot_row = row-1
            print ("spot row determined as", spot_row)
            print ("Lower LTP = ", oc_array[spot_row, 11])
            print ("Upper LTP = ", oc_array[row, 11])  
            break

    row = 0
    result_array_col = 0
    
# Build Output Array for Expiry Strike Prices
    for row in range(max_rows-1):
        result_array[row, result_array_col] = round(float(oc_array[row, 11]),0)

# Build Header Info    
    result_array_col += 1
    result_array_hdr[0,0] = "LTP = " + str(int(LTP))
    result_array_hdr[1,0] = "Trade 1"
    result_array_hdr[2,0] = "Trade 2"
    result_array_hdr[3,0] = "Trade 3"
    result_array_hdr[4,0] = "Trade 4"
    result_array_hdr[5,0] = "Initial Flow"
    result_array_hdr[6,0] = "Expiry Strike"

# Process Bull Put Spread - parameters are for ITM Sell above spot_row and OTM Buy below spot_row
    result_array_hdr[0,1] = "Bull Put Spread"
    bull_put_spread(1,0)
    result_array_col += 1

# Process Bear Put Spread - parameters are for ITM Buy above spot_row and OTM Sell below spot_row
    result_array_hdr[0,2] = "Bear Put Spread"
    bear_put_spread(1,0)
    result_array_col += 1

# Process Bull Call Spread - parameters are for ITM Buy (Mostly AT the Money) on spot_row and OTM Sell below spot_row
    result_array_hdr[0,3] = "Bull Call Spread"
    bull_call_spread(0,1)
    result_array_col += 1

# Process Bear Call Spread - parameters are for ITM Sell on spot_row and OTM Buy below spot_row
    result_array_hdr[0,4] = "Bear Call Spread"
    bear_call_spread(0,1)
    result_array_col += 1

# Process Call Ratio Back Spread - parameters are for ITM Sell and Buy two OTM Call Options (CE)
    result_array_hdr[0,5] = "Call Ratio Back Spread"
    call_ratio_back_spread(-1,2)
    result_array_col += 1

# Process Put Ratio Back Spread - parameters are for ITM Sell and Buy two OTM Put Options (PE)
    result_array_hdr[0,6] = "Put Ratio Back Spread"
    put_ratio_back_spread(2,-2)
    result_array_col += 1
    
# Write result set to csv and exit.. 
def wrapup():
    np.savetxt("result_set.csv",np.vstack((result_array_hdr,result_array)) ,delimiter=',',fmt="%s")
    print("Completed..")


# Strategy Routines...
   
# Bull Put Spread is Good when:
    # The markets have declined considerably (therefore PUT premiums have swelled)
    # The volatility is on the higher side
    # There is plenty of time to expiry
    # And you have a moderately bullish outlook looking ahead
    #  
    # Net Credit Strategy
    # 
    # To implement the bull put spread –
    # Buy 1 "Out of the Money" Put option (leg 1)
    # Sell 1 "In the Money" Put option (leg 2)
    # When you do this ensure:
    # All strikes belong to the same underlying
    # Belong to the same expiry series
    # Each leg involves the same number of options
def bull_put_spread(ITM_up, OTM_Down):

    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_credit
    print ("Calculating bull_put_spread..")
    Instrument_Type = 'PE'
    ITM_sell_row = spot_row + ITM_up
    OTM_buy_row = spot_row + OTM_Down
    ITM_Strike = oc_array[ITM_sell_row, 11]
    OTM_Strike = oc_array[OTM_buy_row, 11]

    print("ITM Sell = ", ITM_Strike)
    print("OTM Buy = ", OTM_Strike)

    prem_received_for_sell = float(oc_array[ITM_sell_row,17])
    prem_paid_for_buy = float(oc_array[OTM_buy_row,17])
    result_array_hdr[1,1] = "Sell " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[2,1] = "Buy " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[3,1] = "N/A"
    result_array_hdr[4,1] = "N/A"

    initial_credit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Credit = ", initial_credit)
    result_array_hdr[5,1] = str(round(initial_credit,2))

    blps_on_expiry()
    print("bull_put_spread processing completed.. \n \n")
def blps_on_expiry():
    print ("determining intrinsic value on expiry for bull_put_spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)
    blps_iv_sp = []
    blps_iv_bp = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        blps_iv_sp.append(-1*(max((ITM_Strike - expiry_strike),0)))
        blps_iv_bp.append(max((OTM_Strike - expiry_strike),0))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(blps_iv_bp[j])+float(blps_iv_sp[j])+initial_credit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    # print ("Expiry, OTM PE Value (Bought), ITM PE Value (Sold), P/L at Expiry")
    # i = 0
    
    # for i in range(j):
    #     print (expiry[i], ",", blps_iv_bp[i],  ",", blps_iv_sp[i],  ",", value_at_expiry[i])
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")

# Bear Put Spread is Good when:
    # when the market outlook is moderately bearish , 
    # i.e you expect the market to go down in the near term while at the same time you don’t expect it to go down much
    #  
    # Net Debit Strategy
    # 
    # To implement the bull put spread –
    # Buying an In the money Put option
    # Selling an Out of the Money Put option
    # 
    # When you do this ensure:
    # All strikes belong to the same underlying
    # Belong to the same expiry series
    # Each leg involves the same number of options
def bear_put_spread(ITM_up, OTM_Down):

    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_debit
    print ("Calculating bear_put_spread..")
    Instrument_Type = 'PE'
    ITM_buy_row = spot_row + ITM_up
    OTM_sell_row = spot_row + OTM_Down
    OTM_Strike = oc_array[OTM_sell_row, 11]
    ITM_Strike = oc_array[ITM_buy_row, 11]

    print("ITM Buy = ", ITM_Strike)
    print("OTM Sell = ", OTM_Strike)
    
    prem_received_for_sell = float(oc_array[OTM_sell_row,17])
    prem_paid_for_buy = float(oc_array[ITM_buy_row,17])

    result_array_hdr[1,2] = "Buy " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[2,2] = "Sell " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[3,2] = "N/A"
    result_array_hdr[4,2] = "N/A"

    initial_debit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Debit = ", initial_debit)
    result_array_hdr[5,2] = str(round(initial_debit,2))

    brps_on_expiry()
    print("bear_put_spread processing completed.. \n \n")
def brps_on_expiry():
    print ("determining intrinsic value on expiry for bear_put_spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)

    brps_iv_s1 = []
    brps_iv_b1 = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        brps_iv_s1.append(max((ITM_Strike - expiry_strike),0))
        brps_iv_b1.append(-1*(max((OTM_Strike - expiry_strike),0)))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(brps_iv_b1[j])+float(brps_iv_s1[j])+initial_debit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")

# Bull Call spread is Good when:
    # when the market outlook is moderately bullish and expiry is in close by
    #  
    # Net Credit Strategy
    # 
    # To implement the bull call spread –
    # Buy 1 ITM call option (leg 1)
    # Sell 1 OTM call option (leg 2)
    # 
    # When you do this ensure:
    # All strikes belong to the same underlying
    # Belong to the same expiry series
    # Each leg involves the same number of options
def bull_call_spread(ITM_up, OTM_Down):
    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_debit
    print ("Calculating bull_call_spread..")
    Instrument_Type = 'CE'
    ITM_buy_row = spot_row + ITM_up
    OTM_sell_row = spot_row + OTM_Down
    OTM_Strike = oc_array[OTM_sell_row, 11]
    ITM_Strike = oc_array[ITM_buy_row, 11]

    print("ITM Buy = ", ITM_Strike)
    print("OTM Sell = ", OTM_Strike)
    
    prem_received_for_sell = float(oc_array[OTM_sell_row,5])
    prem_paid_for_buy = float(oc_array[ITM_buy_row,5])

    result_array_hdr[1,3] = "Buy " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[2,3] = "Sell " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[3,3] = "N/A"
    result_array_hdr[4,3] = "N/A"

    initial_debit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Debit = ", initial_debit)
    result_array_hdr[5,3] = str(round(initial_debit,2))

    blcs_on_expiry()
    print("bull_call_spread processing completed.. \n \n")
def blcs_on_expiry():
    print ("determining intrinsic value on expiry for bull_call_spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)

    brps_iv_s1 = []
    brps_iv_b1 = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        brps_iv_b1.append(max((expiry_strike - ITM_Strike),0))
        brps_iv_s1.append(-1*(max((expiry_strike - OTM_Strike),0)))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(brps_iv_b1[j])+float(brps_iv_s1[j])+initial_debit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")

# Bear Call Spread is Good when:
    # when the market outlook is moderately bearish 
    # The markets have rallied considerably (therefore CALL premiums have swelled)
    # The volatility is favorable
    # Ample time to expiry
    #  
    # Net Debit Strategy
    # 
    # To implement the bear call spread –
    # Sell 1 ITM call option (leg 1)
    # Buy 1 OTM call option (leg 2)
    # 
    # When you do this ensure:
    # All strikes belong to the same underlying
    # Belong to the same expiry series
    # Each leg involves the same number of options
def bear_call_spread(ITM_up, OTM_Down):
    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_credit
    print ("Calculating bear_call_spread..")
    Instrument_Type = 'CE'
    OTM_buy_row = spot_row + OTM_Down
    ITM_sell_row = spot_row + ITM_up
    ITM_Strike = oc_array[ITM_sell_row, 11]
    OTM_Strike = oc_array[OTM_buy_row, 11]

    print("ITM Sell = ", ITM_Strike)
    print("OTM Buy = ", OTM_Strike)
    
    prem_received_for_sell = float(oc_array[ITM_sell_row,5])
    prem_paid_for_buy = float(oc_array[OTM_buy_row,5])

    result_array_hdr[1,4] = "Sell " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[2,4] = "Buy " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[3,4] = "N/A"
    result_array_hdr[4,4] = "N/A"

    initial_credit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Credit = ", initial_credit)
    result_array_hdr[5,4] = str(round(initial_credit,2))

    brcs_on_expiry()
    print("bear_call_spread processing completed.. \n \n")
def brcs_on_expiry():
    print ("determining intrinsic value on expiry for bear_call_spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)

    brps_iv_s1 = []
    brps_iv_b1 = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        brps_iv_s1.append(-1*(max((expiry_strike - ITM_Strike),0)))
        brps_iv_b1.append(max((expiry_strike - OTM_Strike),0))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(brps_iv_b1[j])+float(brps_iv_s1[j])+initial_credit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")

# Call Ratio Back Spread is Good when:
    # The strategy is deployed when one is out rightly bullish on a stock (or index), 
    # unlike the bull call spread or bull put spread where one is moderately bullish.
    # At a broad level this is what you will experience when you implement the Call Ratio Back Spread-
    # - Unlimited profit if the market goes up
    # - Limited profit if market goes down
    # - A predefined loss if the market stay within a range
    #  
    # Net Credit Strategy
    # 
    # To implement the Call Ratio Back Spread –
    # Sell 1 ITM call option (leg 1)
    # Buy 2 OTM call option (leg 2)
    # 
    # The Call options belong to the same expiry
    # Belongs to the same underlying
    # The ratio of 2:1 is maintained
def call_ratio_back_spread(ITM_up, OTM_Down):
    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_credit
    print ("Calculating call_ratio_back_spread..")
    Instrument_Type = 'CE'
    OTM_buy_row = spot_row + OTM_Down
    ITM_sell_row = spot_row + ITM_up
    ITM_Strike = oc_array[ITM_sell_row, 11]
    OTM_Strike = oc_array[OTM_buy_row, 11]

    print("ITM Sell = ", ITM_Strike)
    print("OTM Buy = ", OTM_Strike)
    
    prem_received_for_sell = float(oc_array[ITM_sell_row,5]) 
    prem_paid_for_buy = float(oc_array[OTM_buy_row,5]) * 2

    result_array_hdr[1,5] = "Sell " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[2,5] = "Buy 2x " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[3,5] = "N/A"
    result_array_hdr[4,5] = "N/A"

    initial_credit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Credit = ", initial_credit)
    result_array_hdr[5,5] = str(round(initial_credit,2))

    crbs_on_expiry()
    print("call ratio back spread processing completed.. \n \n")
def crbs_on_expiry():
    print ("determining intrinsic value on expiry for call ratio back spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)

    brps_iv_s1 = []
    brps_iv_b1 = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        brps_iv_s1.append(-1*(max((expiry_strike - ITM_Strike),0)))
        brps_iv_b1.append(2*max((expiry_strike - OTM_Strike),0))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(brps_iv_b1[j])+float(brps_iv_s1[j])+initial_credit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")

# Put Ratio Back Spread is Good when:
    # The strategy is deployed when one is out rightly bearish on a stock (or index), 
    # unlike the bull call spread or bull put spread where one is moderately bullish.
    # At a broad level this is what you will experience when you implement the put Ratio Back Spread-
    # - Unlimited profit if the market goes down
    # - Limited profit if market goes up
    # - A predefined loss if the market stays within a range
    #  
    # Net Credit Strategy
    # 
    # To implement the put Ratio Back Spread –
    # Sell 1 ITM put option (leg 1)
    # Buy 2 OTM put option (leg 2)
    # 
    # The Put options belong to the same expiry
    # Belongs to the same underlying
    # The ratio of 2:1 is maintained
def put_ratio_back_spread(ITM_up, OTM_Down):
    global spot_row
    global oc_array
    global ITM_Strike
    global OTM_Strike
    global result_array
    global result_array_col
    global initial_credit
    print ("Calculating put_ratio_back_spread..")
    Instrument_Type = 'PE'
    OTM_buy_row = spot_row + OTM_Down
    ITM_sell_row = spot_row + ITM_up
    ITM_Strike = oc_array[ITM_sell_row, 11]
    OTM_Strike = oc_array[OTM_buy_row, 11]

    print("ITM Sell = ", ITM_Strike)
    print("OTM Buy = ", OTM_Strike)
    
    prem_received_for_sell = float(oc_array[ITM_sell_row,17]) 
    prem_paid_for_buy = float(oc_array[OTM_buy_row,17]) * 2

    result_array_hdr[1,6] = "Sell " + str(int(ITM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_received_for_sell),1))
    result_array_hdr[2,6] = "Buy 2x " + str(int(OTM_Strike)) + " " + Instrument_Type + " @"  + str(round(float(prem_paid_for_buy),1))
    result_array_hdr[3,6] = "N/A"
    result_array_hdr[4,6] = "N/A"

    initial_credit = prem_received_for_sell - prem_paid_for_buy

    print ("Initial Credit = ", initial_credit)
    result_array_hdr[5,6] = str(round(initial_credit,2))

    prbs_on_expiry()
    print("put ratio back spread processing completed.. \n \n")
def prbs_on_expiry():
    print ("determining intrinsic value on expiry for put ratio back spread.. \n")

    least_strike_row = int(0)
    highest_strike_row = int(max_rows)

    brps_iv_s1 = []
    brps_iv_b1 = []
    expiry = []
    value_at_expiry = []
    i = least_strike_row
    j = 0

    
    while i < highest_strike_row-1:
        expiry_strike = oc_array[i,11]
        brps_iv_s1.append(-1*(max((ITM_Strike - expiry_strike),0)))
        brps_iv_b1.append(2*max((OTM_Strike - expiry_strike),0))
        expiry.append(oc_array[i,11])
        value_at_expiry.append(round(float(brps_iv_b1[j])+float(brps_iv_s1[j])+initial_credit,2))
        result_array[j,result_array_col] = round(float(value_at_expiry[j])*LTQ,2)
        
        j+=1
        i+=1
    max_profit = max(value_at_expiry)
    max_loss = min(value_at_expiry)
    
    print("\n")
    print ("Max Profit = ", max_profit*LTQ)
    print ("Max Loss = ", max_loss*LTQ, "\n")
    print ("Intrinsic Value at Expiry determined.. \n")


if __name__ == "__main__":
  main()
  wrapup()
  